export interface IOption {
  label: string;
  value: boolean;
}

export interface IMovie {
  id: number;
  title: string;
  director: string;
  summary: string;
  genres: string[];
}